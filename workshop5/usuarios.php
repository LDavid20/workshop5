<?php
  require('functions.php');
class Usuario
{  

    function getUser() {
        $conn = getConnection();
        $sql = "SELECT * FROM users";
        $result = $conn->query($sql);
      
        if ($conn->connect_errno) {
          $conn->close();
          return [];
        }
        $conn->close();
        /*FORMATO*/ 
        $users = "";
        foreach($result as $user){
            $users .="{$user['id']} , {$user['name']}, {$user['lastname']}, {$user['cedula']}, {$user['age']}\n";
        }
        return  $users;
        
    }

    function getUserCant($cant) {
        $conn = getConnection();
        $sql = "SELECT * FROM users LIMIT $cant";
        $result = $conn->query($sql);
      
        if ($conn->connect_errno) {
          $conn->close();
          return [];
        }
        $conn->close();
        /*FORMATO*/ 
        $usershtml = "";
        foreach($result as $user){
            $usershtml .="{$user['id']} , {$user['name']}, {$user['lastname']}, {$user['cedula']}, {$user['age']}\n";
        }
        return  $usershtml;
        
    }
}

